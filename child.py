import socket
import random
import sys

services = ["wind.erfan-thesis.online", "temp.erfan-thesis.online", "humidity.erfan-thesis.online"]

middlelayers= [ "middle.temp.erfan-thesis.online", "middle.humidity.erfan-thesis.online", "middle.wind.erfan-thesis.online"]

localserver = "127.0.0.1"

def makeRequest(host, number):

    s = socket.socket()            
    port = 5000                    

    s.connect((host, port))
  
    with open('storage-child/received_file', 'wb') as f:
        print ('file opened')
        while True:
            
            data = s.recv(1024)
            if not data:
                break
            f.write(data)

    f.close()
    print('Successfully get the file')
    s.close()
    print('connection closed')

def tradition():
    counter =0 
    while counter < 1000: 
        randomService = random.randrange(0, len(services))
        makeRequest(services[randomService], randomService)
        counter = counter + 1
def test():
    randomService = random.randrange(0, len(services))
    print (services[randomService])
    makeRequest(localserver, 0) #services[randomService], randomService)

def smart():
    counter =0 
    while counter < 1000: 
        randomService = random.randrange(0, len(middlelayers))
        makeRequest(middlelayers[randomService], randomService)
        counter = counter + 1
    
def main(argument):
    if argument == 'tradition':
        tradition()
    
    elif sys.argv[1] == 'smart':
        smart()

    elif sys.argv[1] == 'test':
        test()
    


if __name__ == '__main__':
    main(sys.argv[1])
