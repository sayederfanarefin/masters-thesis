import paramiko
import sys

privateKey="/home/erfan/.ssh/id_rsa"

commandsUpdate = ["sudo apt update -y", "sudo apt upgrade -y"]
commandsGitPull= [ "cd python-file-transfer", "git pull origin master"]
# commandsGitPull= [ "cd python-file-transfer", "git clone https://github.com/sayederfanarefin/python-file-transfer.git"]

clientVms = [
    "client1.temp.erfan-thesis.online",
    "client2.temp.erfan-thesis.online",
    "client3.temp.erfan-thesis.online",
    "client1.humidity.erfan-thesis.online",
    "client2.humidity.erfan-thesis.online",
    "client3.humidity.erfan-thesis.online",
    "client1.wind.erfan-thesis.online",
    "client2.wind.erfan-thesis.online"
]

middleVms = [
    "middle.temp.erfan-thesis.online",
    "middle.humidity.erfan-thesis.online",
    "middle.wind.erfan-thesis.online"
]

mainVms = [
    "temp.erfan-thesis.online",
    "humidity.erfan-thesis.online",
    "wind.erfan-thesis.online"
]

def sshVM(server, commands):
    ssh = paramiko.SSHClient()
    ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    key = paramiko.RSAKey.from_private_key_file(privateKey) 
    ssh.connect(server, username="erfan", pkey=key)
    i =0
    while i < len (commands):
        print ("======> " + commands[i])
        ssh_stdin, ssh_stdout, ssh_stderr = ssh.exec_command(commands[i])
        print (ssh_stdout.readlines())
        i = i+1
    ssh.close()

def startMainVms():
    i=0
    while i < len(mainVms):
        print ("=========================> VM:" + mainVms[i])
        if 'temp' in mainVms[i]:
            sshVM(mainVms[i], ["cd python-file-transfer", "nohup python3 mother.py temp &"])
        elif 'wind' in mainVms[i]:
            sshVM(mainVms[i], ["cd python-file-transfer", "nohup python3 mother.py wind &"])
        elif 'humidity' in mainVms[i]:
            sshVM(mainVms[i], ["cd python-file-transfer", "nohup python3 mother.py humidity &"])
        
        i = i+1

def main():
    if sys.argv[1] == 'start':
        if sys.argv[2] == 'main':
            #start main servers
            startMainVms()

    

if __name__ == '__main__':
    main()
