#https://www.bogotobogo.com/python/python_network_programming_server_client_file_transfer.php

import socket                   # Import socket module
import sys

file20 = 'storage-mother/dummy-data-20MB'
file50 = 'storage-mother/dummy-data-50MB'
file5 = 'storage-mother/dummy-data-5MB'

port = 5000                    # Reserve a port for your service.
s = socket.socket()             # Create a socket object
host = socket.gethostname()     # Get local machine name
s.bind((host, port))            # Bind to the port
s.listen(5)                     # Now wait for client connection.

print ('Server listening....' + sys.argv[1])

while True:
   conn, addr = s.accept()     # Establish connection with client.
   print ('Got a request')
   data = conn.recv(1024)

   filename='dummydaya50MB.zip'

   if sys.argv[1] == 'temp':
      filename = file5

   elif sys.argv[1] == 'humidity':
      filename = file20

   elif sys.argv[1] == 'wind':
      filename = file50


   f = open(filename,'rb')
   l = f.read(1024)
   while (l):
      conn.send(l)
      l = f.read(1024)
   f.close()

   print('Done sending')
   conn.close()